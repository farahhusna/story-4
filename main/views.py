from django.shortcuts import render


def farah(request):
    return render(request, 'main/farah.html')

def identitas(request):
    return render(request, 'main/identitas.html')

def experience(request):
    return render(request, 'main/experience.html')
    
