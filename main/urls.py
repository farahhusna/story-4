from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.farah, name='farah'),
    path('experience/', views.experience, name='experience'),  
    path('profile/', views.identitas, name='identitas'), 
]
