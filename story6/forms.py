from django import forms


class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
        label="Nama kegiatan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60',
                'placeholder' : 'Ketik disini',
                'type' : 'text',
            }
        )
    )


class FormPeserta(forms.Form):
    nama_peserta = forms.CharField(
        label="Nama Lengkap",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder' : 'Ketik disini',
                'type' : 'text',
            }
        )
    )
