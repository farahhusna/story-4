from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Kegiatan, Peserta
from django.apps import apps
from .views import index, addkegiatan, listkegiatan, addpeserta
from .forms import FormKegiatan, FormPeserta
from .apps import Story6Config

class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(nama_kegiatan="Belajar Bersama")
        self.peserta = Peserta.objects.create(nama_peserta="Farah")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Peserta.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "Belajar Bersama")
        self.assertEqual(str(self.peserta), "Farah")



class UrlsTest(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(nama_kegiatan="Belajar Bersama")
        self.peserta = Peserta.objects.create(nama_peserta="Husna", kegiatan=Kegiatan.objects.get(nama_kegiatan="Belajar Bersama"))
        self.index = reverse("index")
        self.listkegiatan = reverse("listkegiatan")
        self.addkegiatan = reverse("addkegiatan")
        self.addpeserta = reverse("addpeserta", args=[self.kegiatan.pk])
        
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_addkegiatan_use_right_function(self):
        found = resolve(self.addkegiatan)
        self.assertEqual(found.func, addkegiatan)

    def test_listkegiatan_use_right_function(self):
        found = resolve(self.listkegiatan)
        self.assertEqual(found.func, listkegiatan)

    def test_addpeserta_use_right_function(self):
        found = resolve(self.addpeserta)
        self.assertEqual(found.func, addpeserta)



class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("index")
        self.listkegiatan = reverse("listkegiatan")
        self.addkegiatan = reverse("addkegiatan")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_GET_listkegiatan(self):
        response = self.client.get(self.listkegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listkegiatan.html')

    def test_GET_addkegiatan(self):
        response = self.client.get(self.addkegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addkegiatan.html')

    def test_POST_addkegiatan(self):
        response = self.client.post(self.addkegiatan,
                                    {
                                        'nama_kegiatan': 'Belajar Bersama'
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addkegiatan_invalid(self):
        response = self.client.post(self.addkegiatan,
                                    {
                                        'nama_kegiatan': ''
                                    }, follow=True)
        self.assertTemplateUsed(response, 'addkegiatan.html')


class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="abc")
        kegiatan.save()

    def test_addpeserta_POST(self):
        response = Client().post('/story6/addpeserta/1/',
                                 data={'nama_peserta': 'Nabilah'})
        self.assertEqual(response.status_code, 302)

    def test_addpeserta_GET(self):
        response = self.client.get('/story6/addpeserta/1/')
        self.assertTemplateUsed(response, 'addpeserta.html')
        self.assertEqual(response.status_code, 200)

    def test_addpeserta_POST_invalid(self):
        response = Client().post('/story6/addpeserta/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'addpeserta.html')


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')

