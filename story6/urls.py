from django.contrib import admin
from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('addkegiatan/',views.addkegiatan,name='addkegiatan'),
    path('listkegiatan/', views.listkegiatan, name='listkegiatan'),
    path('addpeserta/<int:org_id>/', views.addpeserta, name='addpeserta'),
]
