from django.shortcuts import render, redirect

# Create your views here.
from .forms import FormKegiatan, FormPeserta
from .models import Kegiatan, Peserta

def index(request):
    return render(request,'index.html')

def addkegiatan(request):
    form_kegiatan = FormKegiatan()
    if request.method == "POST":
        form_input1 = FormKegiatan(request.POST)
        if form_input1.is_valid():
            kegiatan = Kegiatan()
            kegiatan.nama_kegiatan = form_input1.cleaned_data['nama_kegiatan']
            kegiatan.save()
            return redirect('/story6/addkegiatan')
        else:
            return render(request, 'addkegiatan.html', {'form': form_kegiatan, 'status': 'failed'})
    else:
        return render(request, 'addkegiatan.html', {'form': form_kegiatan})

def listkegiatan(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    return render(request, 'listkegiatan.html', {'kegiatan': kegiatan, 'peserta': peserta})

def addpeserta(request, org_id):
    form_peserta = FormPeserta()
    if request.method == "POST":
        form_input2 = FormPeserta(request.POST)
        if form_input2.is_valid():
            peserta = Peserta()
            peserta.nama_peserta = form_input2.cleaned_data['nama_peserta']
            peserta.kegiatan = Kegiatan.objects.get(id=org_id)
            peserta.save()
            return redirect('/story6/listkegiatan')
        else:
            return render(request, 'addpeserta.html', {'form': form_peserta, 'status': 'failed'})
    else:
        return render(request, 'addpeserta.html', {'form': form_peserta})
