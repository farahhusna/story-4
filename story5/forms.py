from django import forms

class FormSched(forms.Form) :
    course = forms.CharField(
        max_length = 100,
        widget=forms.TextInput(
            attrs={
            'class' : 'form-control',
            'placeholder' : 'Course',
            'type' : 'text',
            'required' : True
    }))

    lecturer = forms.CharField(
        widget=forms.TextInput(
            attrs={
            'class' : 'form-control',
            'placeholder' : 'Lecturer',
            'type' : 'text',
            'required' : True
    }))

    sks = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : "Number of Credits",
                'type' : 'number',
                'required' : True
    }))

    place = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Place',
                'type' : 'text',
                'required' : True
    }))


    year = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Ex: Gasal 2017/2018',
                'type' : 'text',
                'required' : True
    }))


    description = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Description',
                'type' : 'text',
                'required' : True
    }))
