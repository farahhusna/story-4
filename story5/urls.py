from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.add, name='add'),
    path('ListSchedule/', views.ScheduleList ,name = 'ScheduleList'),
    path('detail/P<int:pk1>/',views.detail, name='detail'),
    path('delete/P<int:pk>/', views.delete, name='delete')
    
]
