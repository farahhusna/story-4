from django.db import models

# Create your models here.
class Schedule (models.Model) :
    course = models.CharField(max_length=50, blank=False)
    lecturer = models.CharField(max_length=50, blank=False)
    sks = models.IntegerField(blank=False)
    place = models.CharField(max_length=20, blank=False)
    year = models.CharField(max_length=20, blank=False)
    description = models.CharField(max_length=150, blank=False)

    