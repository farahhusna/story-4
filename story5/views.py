from django.shortcuts import render, redirect
from .forms import FormSched
from .models import Schedule

# Create your views here.
def add(request) :
    if request.method == 'POST':
        form = FormSched(request.POST)
        if (form.is_valid()) :
            ScheduleFix = Schedule()
            ScheduleFix.course = form.cleaned_data['course']
            ScheduleFix.lecturer = form.cleaned_data['lecturer']
            ScheduleFix.sks = form.cleaned_data['sks']
            ScheduleFix.place = form.cleaned_data['place']
            ScheduleFix.year = form.cleaned_data['year']
            ScheduleFix.description = form.cleaned_data['description']
            ScheduleFix.save()
        return redirect('/story5')
    else :
        form = FormSched()
        schedule = Schedule.objects.all()
        context = {
            'Form' : form,
            'schedule' : schedule
        }
        return render(request, 'addschedule.html', context)

def ScheduleList(request) :
    schedule = Schedule.objects.all()
    return render(request, 'listschedule.html', {'listschedule':schedule})


def delete(request, pk):
    try:
        sched_delete = Schedule.objects.get(pk = pk)
        sched_delete.delete()
        return redirect('/story5/ListSchedule')
    except:
        return redirect('/story5/ListSchedule')

def detail(request, pk1):
    try:
        sched_detail = Schedule.objects.get(pk = pk1)
        context = {
            'sched':sched_detail
        }
        return render(request, 'detailschedule.html',context)
    except:
        return redirect('/story5/ListSchedule')
